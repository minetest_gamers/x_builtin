------------
-- x_builtin Mod for Minetest by SaKeL
-- @author Juraj Vajda
-- @license GNU LGPL 2.1
----

local core = core

core.nodedef_default.wield_scale = {x=1.25, y=1.25, z=1.25}
core.craftitemdef_default.wield_scale = {x=1.5, y=1.5, z=1.5}
core.tooldef_default.wield_scale = {x=1.5, y=1.5, z=1.5}
core.noneitemdef_default.wield_scale = {x=1.25, y=1.25, z=1.25}

print("[Mod] x_builtin loaded..")